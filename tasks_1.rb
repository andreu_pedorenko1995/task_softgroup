require 'rubocop'
#
class MySolution
  def initialize
    @arr_first = [1, 4, 3, 2]
    @arr_second = [1, 4, 5, 7, 8, 10, 20]
    @arr_string = ['Fizz::Buzz::Wizz', 'Fizz::Buzz', 'Fizz']
  end

  def first_task
    sum = 0
    print 'Type the numbers X : '
    x = gets.to_i
    @arr_first.sort.reverse.each_with_index do |e, i|
      p e
      sum = sum + e
      return i + 1 if sum >= x
    end
    return nil
  end

  def index_array
    p '[1,4,5,7,8,10,20]'
    print 'Enter the element arrays : '
    x = gets.to_i
    d = @arr_second.sort.index(x)
    p "Index = #{d}"
  end

  def turn_the_string
    p @arr_string.push('Fizz::Buzz::Wizz')
  end

  def sum
    p (1..100).inject { |sum, n| sum + n }
  end
end

puts 'Welcome to my progrmm'
puts 'To run the program, press 1 to exit, press 0'
bool = true
while bool
  start_number = gets.to_i
  if start_number == 1
    mysolution = MySolution.new
    puts '1 - first_task'
    puts '2 - index_array'
    puts '3 - turn_the_string'
    puts '4 - sum'
    puts '0. exit'
    while bool
      case gets.chomp
      when '1' then p mysolution.first_task
      when '2' then mysolution.index_array
      when '3' then mysolution.turn_the_string
      when '4' then mysolution.sum
      when '0' then
        puts 'Ok...bye'
        bool = false
      end
    end
  end
end