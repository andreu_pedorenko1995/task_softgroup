require "sequel"

DB = Sequel.sqlite

DB.create_table :users do
  primary_key :id
  String :name
  String :email
  DateTime :birthday
end

DB.create_table :posts do
  primary_key :post_id
  foreign_key :user_id, :users
  String :title
  Text :body
end

users = DB[:users]
posts = DB[:posts]

and_id = users.insert(name: 'Andriy', email: 'andriy@gmail.com', birthday: '1991-09-07')
ros_id = users.insert(name: 'Rostik', email: 'rostik@gmail.com', birthday: '2009-05-01')
vlad_id = users.insert(name: 'Vlad', email: 'vlad@gmail.com', birthday: '1996-05-02')
ant_id = users.insert(name: 'Anatoliy', email: 'anatoliy@gmail.com', birthday: '1994-02-08')
mak_id = users.insert(name: 'Maksim', email: 'maksim@gmail.com', birthday: '2002-07-02')
fed_id = users.insert(name: 'Fedir', email: 'fedir@gmail.com', birthday: '2003-04-03')

posts.insert(user_id: and_id, title: 'Lorem ipsum dolor', body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
posts.insert(user_id: ros_id, title: 'Lorem ipsum dolor', body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
posts.insert(user_id: vlad_id, title: 'Lorem ipsum dolor', body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
posts.insert(user_id: ant_id, title: 'Lorem ipsum dolor', body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
posts.insert(user_id: mak_id, title: 'Lorem ipsum dolor', body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
posts.insert(user_id: fed_id, title: 'Lorem ipsum dolor', body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')


# Write SQL query to get all users older than 18 years
puts 'Are people over 18!'
puts users.where { birthday < (Date.today - (18*365)) }.all 
puts '______________________________________________________________________'
# Write SQL query to get all users with their posts
puts 'Posts users'
puts users.graph(:posts, user_id: :id).select(:name, :id, :title, :body).all
